﻿using NUnit.Framework;
using RandomProductApp.Model.Products;

namespace RandomProductApp.NUnitTest.Model.Products
{
    public class ProductTests
    {
        [Test]
        public void GetPrice_Shuriken_ShouldReturnPrice()
        {
            var shuriken = new Shurikens();

            Assert.NotNull(shuriken.GetPricePerUnit());
        }

        [Test]
        public void GetPrice_BagOfPogs_ShouldReturnPrice()
        {
            var bagsOfPogs = new BagOfPogs();

            Assert.NotNull(bagsOfPogs.GetPricePerUnit());
        }

        [Test]
        public void GetPrice_LargeBowlOfTrifle_ShouldReturnPrice()
        {
            var largeBowl = new LargeBowlOfTrifle();

            Assert.NotNull(largeBowl.GetPricePerUnit());
        }

        [Test]
        public void GetPrice_PaperMask_ShouldReturnPrice()
        {
            var paperMask = new PaperMask();

            Assert.NotNull(paperMask.GetPricePerUnit());
        }
    }
}
