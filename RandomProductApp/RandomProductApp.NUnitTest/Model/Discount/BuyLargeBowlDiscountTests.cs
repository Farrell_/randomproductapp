﻿using System.Collections.Generic;
using RandomProductApp.Model.Products;
using NUnit.Framework;
using RandomProductApp.Model.Discounts;

namespace RandomProductApp.NUnitTest.Model.Discount
{
    public class BuyLargeBowlDiscountTests
    {
        [Test]
        public void GetDiscount_EmptyList()
        {
            // Arrange
            var largeBowlDiscount = new BuyLargeBowlDiscount();
            var products = new List<IProduct>();

            // Act
            var discount = largeBowlDiscount.GetDiscount(products);

            // Assert
            Assert.AreEqual(0, discount.Amount);
        }

        [Test]
        public void GetDiscount_OneLargeBowl()
        {
            // Arrange
            var largeBowlDiscount = new BuyLargeBowlDiscount();
            var products = new List<IProduct> { new LargeBowlOfTrifle()};

            // Act
            var discount = largeBowlDiscount.GetDiscount(products);

            // Assert
            Assert.AreEqual(2.75, discount.Amount);
        }

        [Test]
        public void GetDiscount_OneLargeBowl_CountProducts()
        {
            // Arrange
            var largeBowlDiscount = new BuyLargeBowlDiscount();
            var products = new List<IProduct> { new LargeBowlOfTrifle() };

            // Act
            var discount = largeBowlDiscount.GetDiscount(products);

            // Assert
            Assert.AreEqual(2, products.Count);
        }
    }
}
