﻿using NUnit.Framework;
using RandomProductApp.Model.Discounts;
using RandomProductApp.Model.Products;
using System.Collections.Generic;

namespace RandomProductApp.NUnitTest.Model.Discount
{
    public class MoreHundredShurikensDiscountTests
    {
        [Test]
        public void GetDiscount_EmptyList()
        {
            // Arrange
            var shurikensDiscount = new MoreHundredShurikensDiscount();
            var products = new List<IProduct>();

            // Act
            var discount = shurikensDiscount.GetDiscount(products);

            // Assert
            Assert.AreEqual(0, discount.Amount);
        }
        
        [Test]
        public void GetDiscount_OneHundredShurikens()
        {
            // Arrange
            var shurikensDiscount = new MoreHundredShurikensDiscount();

            var products = new List<IProduct>();
            for (int i = 0; i < 100; i++)
            {
                products.Add(new Shurikens());
            }

            // Act
            var discount = shurikensDiscount.GetDiscount(products);

            // Assert
            // (100 * 8.95) * 0.3 = 268.5
            Assert.AreEqual((Shurikens.Price.Amount * 100 ) * 0.3M, discount.Amount);
        }

        [Test]
        public void GetDiscount_OneHundredShurikensPlusOneLargeBowl_Cost()
        {
            // Arrange
            var shurikensDiscount = new MoreHundredShurikensDiscount();

            var products = new List<IProduct>();
            for (int i = 0; i < 100; i++)
            {
                products.Add(new Shurikens());
            }
            products.Add(new LargeBowlOfTrifle());

            // Act
            var discount = shurikensDiscount.GetDiscount(products);

            // Assert
            // ((100 * 8.95) + 2.75) * 0.3 = 269,325
            Assert.AreEqual(((Shurikens.Price.Amount * 100) + LargeBowlOfTrifle.Price.Amount) * 0.3M, discount.Amount);
        }

        [Test]
        public void GetDiscount_OneHundredShurikensPlusOneLargeBowl_CountProducts()
        {
            var products = new List<IProduct>();
            for (int i = 0; i < 100; i++)
            {
                products.Add(new Shurikens());
            }
            products.Add(new LargeBowlOfTrifle());

            // Act
            var discount = new MoreHundredShurikensDiscount().GetDiscount(products);

            // Assert
            // 100 Shurikens + 1 LargeBowlOfTrifle + 1 free Paper mask = 102 items
            Assert.AreEqual(102, products.Count);
        }
    }
}
