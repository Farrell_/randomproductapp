﻿using NUnit.Framework;
using RandomProductApp.Model.Discounts;
using RandomProductApp.Model.Products;
using System.Collections.Generic;

namespace RandomProductApp.NUnitTest.Model.Discount
{
    public class MoreTwoBagsDiscountTests
    {
        [Test]
        public void GetDiscount_EmptyList()
        {
            // Arrange
            var bagsDiscount = new MoreTwoBagsDiscount();
            var products = new List<IProduct>();

            // Act
            var discount = bagsDiscount.GetDiscount(products);

            // Assert
            Assert.AreEqual(0, discount.Amount);
        }

        [Test]
        public void GetDiscount_TwoBags()
        {
            // Arrange
            var bagsDiscount = new MoreTwoBagsDiscount();
            var products = new List<IProduct> { new BagOfPogs(), new BagOfPogs() };

            // Act
            var discount = bagsDiscount.GetDiscount(products);

            // Assert
            Assert.AreEqual((BagOfPogs.Price.Amount * 2) - (BagOfPogs.Price.Amount + (BagOfPogs.Price.Amount * 0.5M)),
                discount.Amount);
        }

        [Test]
        public void GetDiscount_ThreeBags()
        {
            // Arrange
            var bagsDiscount = new MoreTwoBagsDiscount();
            var products = new List<IProduct> { new BagOfPogs(), new BagOfPogs(), new BagOfPogs() };

            // Act
            var discount = bagsDiscount.GetDiscount(products);

            // Assert
            Assert.AreEqual((BagOfPogs.Price.Amount * 3) - (BagOfPogs.Price.Amount + (BagOfPogs.Price.Amount * 2 * 0.5M)),
                discount.Amount);
        }
    }
}
