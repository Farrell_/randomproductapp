﻿using NUnit.Framework;
using RandomProductApp.Model;
using RandomProductApp.Model.Products;

namespace RandomProductApp.NUnitTest.Model
{
    public  class BasketTests
    {
        [Test]
        public void GetTotalPrice_NoProductsAdded_ExpectZero()
        {
            // Arrange
            var basket = new Basket();

            // Act
            var price = basket.GetTotalPrice();

            // Assert
            Assert.AreEqual(0, price.Amount);
        }

        [Test]
        public void GetTotalPrice_AddOneShuriken_ExpectShurikenPrice()
        {
            // Arrange
            var basket = new Basket();
            var shuriken = new Shurikens();
            basket.Add(shuriken);

            // Act
            var price = basket.GetTotalPrice();

            // Assert
            Assert.AreEqual(shuriken.GetPricePerUnit().Amount, price.Amount);
        }

        [Test]
        public void GetTotalPrice_AddOneBagOfPogs_ExpectBagOfPogsPrice()
        {
            // Arrange
            var basket = new Basket();
            var bagOfPogs = new BagOfPogs();
            basket.Add(bagOfPogs);

            // Act
            var price = basket.GetTotalPrice();

            // Assert
            Assert.AreEqual(bagOfPogs.GetPricePerUnit().Amount, price.Amount);
        }

        [Test]
        public void GetTotalPrice_AddTwoPaperMask_ExpectTwoPaperMaskPrice()
        {
            // Arrange
            var basket = new Basket();
            var paperMask = new PaperMask();
            basket.Add(paperMask);
            basket.Add(paperMask);

            // Act
            var price = basket.GetTotalPrice();

            // Assert
            Assert.AreEqual(paperMask.GetPricePerUnit().Amount * 2, price.Amount);
        }

        [Test]
        public void GetTotalPrice_AddHundredShurikens_Expect30PrecentDiscountForHundredShurikensPrice()
        {
            // Arrange
            var basket = new Basket();
            var shurikens = new Shurikens();

            for (int index = 0; index < 100; index++)
            {
                basket.Add(shurikens);
            }

            // Act
            var price = basket.GetTotalPrice();

            // Assert
            Assert.AreEqual(shurikens.GetPricePerUnit().Amount * 100 * 0.7M, price.Amount);
        }

        [Test]
        public void GetTotalPrice_AddTwoBagOfPogs_Expect50PrecentForSecondBagOfPogsDiscount()
        {
            // Arrange
            var basket = new Basket();
            var bagOfPogs = new BagOfPogs();

            basket.Add(bagOfPogs);
            basket.Add(bagOfPogs);

            // Act
            var price = basket.GetTotalPrice();

            // Assert
            Assert.AreEqual(bagOfPogs.GetPricePerUnit().Amount  +
                bagOfPogs.GetPricePerUnit().Amount / 2, price.Amount);
        }
    }
}
