﻿using NUnit.Framework;
using RandomProductApp.Model;

namespace RandomProductApp.NUnitTest
{
    public class MoneyTests
    {
        [Test]
        public void Money_CreateMoney_ReturnsAmountWithPoundCurrency()
        {
            var amount = 2.3M;
            var money = new Money(amount);
                       
            Assert.AreEqual(amount, money.Amount);

            // Test failed
            //Assert.AreEqual(Money.DEFAULT_CURRENCY, "0");
        }
    }
}
