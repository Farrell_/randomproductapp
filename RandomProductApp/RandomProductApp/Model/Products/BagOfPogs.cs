﻿namespace RandomProductApp.Model.Products
{
    public class BagOfPogs : IProduct
    {

        public string Id { get; set; } = "RP-25D-SITB";
        public string Description { get; set; } = "25 Random pogs designs";
        public static Money Price { get; set; } = new Money(5.31M);

        public Money GetPricePerUnit()
        {
            return Price;
        }
    }
}
