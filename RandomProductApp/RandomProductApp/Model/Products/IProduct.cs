﻿namespace RandomProductApp.Model.Products
{
    public interface IProduct
    {
        Money GetPricePerUnit();
    }
}
