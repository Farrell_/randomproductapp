﻿namespace RandomProductApp.Model.Products
{
    public class LargeBowlOfTrifle : IProduct
    {
        public string Id { get; set; } = "RP-1TB-EITB";
        public string Description { get; set; } = "Large collectors edition bowl of Trifle";
        public static Money Price { get; set; } = new Money(2.75M);

        public Money GetPricePerUnit()
        {
            return Price;
        }
    }
}
