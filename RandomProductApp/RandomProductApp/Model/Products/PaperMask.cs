﻿namespace RandomProductApp.Model.Products
{
    public class PaperMask : IProduct
    {
        public string Id { get; set; } = "RP-RPM-FITB";
        public string Description { get; set; } = "Randomly selected paper mask";
        public static Money Price { get; set; } = new Money(0.30M);

        public Money GetPricePerUnit()
        {
            return Price;
        }
    }
}
