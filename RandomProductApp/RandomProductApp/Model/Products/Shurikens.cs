﻿namespace RandomProductApp.Model.Products
{
    public class Shurikens : IProduct
    {
        public string Id { get; set; } = "RP-5NS-DITB";
        public string Description { get; set; } = "5 pointed Shurikens made from stainless steel";
        public static Money Price { get; set; } = new Money(8.95M);

        public Money GetPricePerUnit()
        {
            return Price;
        }
    }
}
