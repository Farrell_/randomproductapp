﻿using RandomProductApp.Model.Discount;
using RandomProductApp.Model.Discounts;
using RandomProductApp.Model.Products;
using RandomProductApp.Extensions;
using System.Collections.Generic;
using System.Linq;
using System;

namespace RandomProductApp.Model
{
    public class Basket
    {
        private readonly IList<IProduct> productStore = new List<IProduct>();
        private readonly IList<IDiscount> discounts = new List<IDiscount> { new BuyLargeBowlDiscount(),
                                                                            new MoreHundredShurikensDiscount(),
                                                                            new MoreTwoBagsDiscount() };

        public void Add(IProduct product)
        {
            productStore.Add(product);
        }

        public Money GetTotalPrice()
        {
            var priceWoDiscounts = productStore.Sum(item => item.GetPricePerUnit());

            var discount = discounts.Sum(item => item.GetDiscount(productStore));

            return priceWoDiscounts - discount;
        }

        public IList<IProduct> GetTotalProducts()
        {
            return productStore;
        }

        public IList<IProduct> GetAllSomeProduct(IProduct product)
        {
            return productStore.Where(item => item.GetType() == product.GetType()).ToList();
        }
    }
}
