﻿using RandomProductApp.Model.Discount;
using RandomProductApp.Model.Products;
using System.Collections.Generic;
using System.Linq;

namespace RandomProductApp.Model.Discounts
{
    public class MoreHundredShurikensDiscount : IDiscount
    {
        public Money GetDiscount(IList<IProduct> products)
        {
            var countOfShurikens = products.Count(item => typeof(Shurikens) == item.GetType());
            decimal price = 0M;

            if (countOfShurikens >= 100)
            {
                var Bagdiscount = new MoreTwoBagsDiscount().GetDiscount(products);
                var buyLargeBowlDiscount = new BuyLargeBowlDiscount().GetDiscount(products);
                var moreHundredShurikensDiscount = countOfShurikens * Shurikens.Price.Amount;


                price = (Bagdiscount.Amount +
                         buyLargeBowlDiscount.Amount +
                         moreHundredShurikensDiscount) * 0.3M;
            }

            return new Money(price);
        }
    }
}
