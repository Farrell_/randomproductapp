﻿using RandomProductApp.Model.Discount;
using RandomProductApp.Model.Products;
using System.Collections.Generic;
using System.Linq;

namespace RandomProductApp.Model.Discounts
{
    public class MoreTwoBagsDiscount : IDiscount
    {
        public Money GetDiscount(IList<IProduct> products)
        {
            var countBagsOfPogs = products.Count(item => typeof(BagOfPogs) == item.GetType());

            // (the sum of all goods) - (discount amount)
            var price = (countBagsOfPogs >= 2)
                ? (countBagsOfPogs * BagOfPogs.Price.Amount) - 
                  (BagOfPogs.Price.Amount + ((countBagsOfPogs - 1) * (BagOfPogs.Price.Amount * 0.5M)))
                : 0;

            return new Money(price); 
        }
    }
}
