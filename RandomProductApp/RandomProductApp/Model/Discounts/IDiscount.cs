﻿using RandomProductApp.Model.Products;
using System.Collections.Generic;

namespace RandomProductApp.Model.Discount
{
    public interface IDiscount
    {
        Money GetDiscount(IList<IProduct> products);
    }
}
