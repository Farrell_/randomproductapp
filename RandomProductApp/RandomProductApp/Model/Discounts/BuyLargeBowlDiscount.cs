﻿using RandomProductApp.Model.Discount;
using RandomProductApp.Model.Products;
using System.Collections.Generic;
using System.Linq;

namespace RandomProductApp.Model.Discounts
{
    public class BuyLargeBowlDiscount : IDiscount
    {
        public Money GetDiscount(IList<IProduct> products)
        {
            var countOfLargeBowl = products.Count(item => typeof(LargeBowlOfTrifle) == item.GetType());

            if (countOfLargeBowl >= 1)
            {
                products.Add(new PaperMask());
                return new Money(LargeBowlOfTrifle.Price.Amount * countOfLargeBowl);
            }
            else
            {
                return new Money(0);
            }
        }
    }
}
